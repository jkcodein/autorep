package com.ocmt.autorep.cucumber.stepdefs;

import com.ocmt.autorep.AutorepApp;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.ResultActions;

import org.springframework.boot.test.context.SpringBootTest;

@WebAppConfiguration
@SpringBootTest
@ContextConfiguration(classes = AutorepApp.class)
public abstract class StepDefs {

    protected ResultActions actions;

}
