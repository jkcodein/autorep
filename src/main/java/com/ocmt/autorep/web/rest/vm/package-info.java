/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ocmt.autorep.web.rest.vm;
